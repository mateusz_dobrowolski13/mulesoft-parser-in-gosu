package com.lv.nucleus.mulesoft.getter

uses java.util.function.Function


class GetterObjectFromLambda<T> implements IGetter<T> {
  private var _fun : Function

  construct(fun : Function) {
    _fun = fun
  }

  override function getObject(coreObject : T) : Optional {
    return Optional.ofNullable(_fun.apply(coreObject))
  }
}