package com.lv.nucleus.mulesoft.conventer

uses com.lv.nucleus.mulesoft.schema.xml.XmlPayload
uses gw.xml.XmlElement


internal class ArrayConventer<T> extends AbstractConventer<T> {

  construct(objectToConvert : Object) {
    super(objectToConvert)
  }

  override function convert(target : XmlPayload<T>) {
      if (ObjectToConvert.Class.isArray() and ObjectToConvert typeis Object[]) {
        ObjectToConvert = ObjectToConvert.where(\elt -> elt != null)
        var xmlArray = new XmlElement(target.CurrentField.Left)
        for (element in ObjectToConvert as Object[] index i) {
         var newObject = new XmlPayload("Entry", element, target.Fields)
          xmlArray.addChild(newObject.XmlElement)
        }
        target.XmlElement.addChild(xmlArray)
      } else {
        throw new IllegalAccessException("You do not provided an array field ${target.CurrentField}.}")
      }
  }
}