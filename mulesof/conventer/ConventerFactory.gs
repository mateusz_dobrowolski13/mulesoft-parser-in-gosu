package com.lv.nucleus.mulesoft.conventer

uses java.util.function.Predicate

class ConventerFactory{

  public static function getConventer<T>(propertyInfo : Object) : AbstractConventer {
    switch (Conventer.AllValues.firstWhere(\elt -> elt._predicate.test(propertyInfo)).Value) {
      case Conventer.CLASS_DTO_CONVENTER:
        return new EntityConventer(propertyInfo)
      case Conventer.ARRAY_DTO_CONVENTER:
        return new ArrayConventer(propertyInfo)
      default:
        return new DefaultFieldConventer(propertyInfo)
    }
  }

  private enum Conventer {
    CLASS_DTO_CONVENTER(\object -> object typeis KeyableBean or object typeis gw.entity.TypeKey),
    ARRAY_DTO_CONVENTER(\object -> object typeis Object[]),

    private var _predicate : Predicate

    private construct(predicate : Predicate) {
      _predicate = predicate
    }
  }
}



