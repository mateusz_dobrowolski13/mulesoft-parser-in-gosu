package com.lv.nucleus.mulesoft.schema

uses com.lv.nucleus.mulesoft.getter.GetterObjectFromLambda
uses com.lv.nucleus.mulesoft.getter.GetterObjectFromStringPath
uses com.lv.nucleus.mulesoft.schema.xml.XmlParseOption
uses com.lv.nucleus.mulesoft.schema.xml.XmlPayload
uses org.apache.commons.lang3.tuple.MutablePair

uses java.util.function.Function
uses java.util.function.Predicate


internal class SchemaBuilder implements Schema {
  private var fields = new ArrayList<MutablePair<String, XmlParseOption>>()
  private var _target : String

  construct(target : String) {
    _target = target
  }

  internal function withField<T extends KeyableBean>(xmlPath : String, entityFieldPath : String, visibilityCondition : Predicate<T> = null) : SchemaBuilder {
    fields.add(MutablePair.of(xmlPath, new XmlParseOption().withEntity(new GetterObjectFromStringPath(entityFieldPath)).withVisibilityCondtion(visibilityCondition)))
    return this
  }

  internal function withFieldLambda<T extends KeyableBean, R>(xmlPath : String, fun : Function<T, R>, visibilityCondition : Predicate<T> = null) : SchemaBuilder {
    fields.add(MutablePair.of(xmlPath, new XmlParseOption().withEntity(new GetterObjectFromLambda(fun)).withVisibilityCondtion(visibilityCondition)))
    return this
  }

  internal function withFieldAndValue(xmlPath : String, value : Object) : SchemaBuilder {
    fields.add(MutablePair.of(xmlPath, new XmlParseOption().withValue(value)))
    return this
  }

  public override function toXml(coreObject : KeyableBean) : String {
    return new XmlPayload(_target, coreObject, fields).XmlElement.asUTFString()
  }
}