package com.lv.nucleus.mulesoft.schema.xml

uses com.lv.nucleus.mulesoft.conventer.ConventerFactory
uses gw.xml.XmlElement
uses org.apache.commons.lang3.tuple.MutablePair

uses java.util.stream.Collectors

class XmlPayload<T> {
  protected var _xml : XmlElement as XmlElement
  private var _coreObject : T as CoreObject
  private var _currentField : MutablePair<String, XmlParseOption>as CurrentField
  private var _declaredFields : List<MutablePair<String, XmlParseOption>>as Fields

  construct(name : String, coreObject : T, inputFields : List<MutablePair<String, XmlParseOption>>) {
    _coreObject = coreObject
    _xml = new XmlElement(name)
    convertToDto(inputFields)
  }

  private function convertToDto(inputFields : List<MutablePair<String, XmlParseOption>>) {
    var groupedFields = inputFields.stream().collect(Collectors.groupingBy(\pair : MutablePair<String, XmlParseOption> -> groupFieldsByName(pair)))
    for (fieldEntry in groupedFields.entrySet()) {
      setFields(fieldEntry)

      if (CurrentField.Right.VisibilityCondtion == null or CurrentField.Right.VisibilityCondtion.test(CoreObject)) {
        var propertyInfo = CurrentField.Right.FieldValueGetter.getObject(CoreObject)
        propertyInfo.ifPresent(\test : Object -> {
          var conventer = ConventerFactory.getConventer(test)
          conventer.convert(this)
        })
      }
    }
  }

  private function groupFieldsByName(pair : MutablePair<String, XmlParseOption>) : String {
    var fieldName = pair.Left.split("\\.", 2)[0]
    return fieldName
  }

  private function setFields(entry : Map.Entry<String, List<MutablePair<String, XmlParseOption>>>) {
    if (entry.Value.hasMatch(\field -> field.Left == entry.Key)) {
      Fields = new ArrayList<MutablePair<String, XmlParseOption<Object, Object>>>()
      entry.Value.where(\elt -> elt.Left != entry.Key).map(\elt -> MutablePair.of(elt.Left, elt.Right)).each(\elt -> {
        elt.Left = elt.Left.split("\\.", 2)[1]
        Fields.add(elt)
      })
      CurrentField = entry.Value.firstWhere(\field -> field.Left == entry.Key)
    } else {
      throw new IllegalArgumentException("There is no provided a new core object for ${entry.Key} field")
    }
  }

}