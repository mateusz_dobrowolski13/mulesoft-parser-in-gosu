package com.lv.nucleus.mulesoft.conventer

uses com.lv.nucleus.mulesoft.schema.xml.XmlPayload

internal class EntityConventer<T> extends AbstractConventer<T> {

  construct(objectToConvert : Object) {
    super(objectToConvert)
  }

  override function convert(target : XmlPayload<T>) {
      var newObject = new XmlPayload(target.CurrentField.Left, ObjectToConvert, target.Fields)
      target.XmlElement.addChild(newObject.XmlElement)
  }

}