package com.lv.nucleus.mulesoft.getter


interface IGetter<T> {
  public function getObject(coreObject: T) : Optional
}