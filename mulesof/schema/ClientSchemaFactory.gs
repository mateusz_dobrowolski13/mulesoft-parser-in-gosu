package com.lv.nucleus.mulesoft.schema


class ClientSchemaFactory {

  public static function Client(client : ClientEnum) : SchemaComposer {
    switch (client) {
      case FRAUD_ANALYTICS_MODEL:
        return new FraudAnalyticsModelSchema()
      case FRAUD_ANALYTICS_MODELv2:
        return new FraudAnalyticsModelSchema2()
    }
  }
}