package com.lv.nucleus.mulesoft.conventer

uses com.lv.nucleus.mulesoft.schema.xml.XmlPayload
uses gw.xml.XmlElement
uses gw.xml.XmlSimpleValue
uses gw.xml.date.XmlDateTime

uses java.math.BigDecimal


internal class DefaultFieldConventer<T> extends AbstractConventer<T> {

  construct(objectToConvert : Object) {
    super(objectToConvert)
  }

  override function convert(target : XmlPayload<T>) {
    try {
      var xmlChild = new XmlElement(target.CurrentField.Left)
      var simpleValue = getSimpleValue(ObjectToConvert)
      if (simpleValue != null) {
        xmlChild.withSimpleValue(simpleValue)
      } else {
        xmlChild.withText(ObjectToConvert.toString())
      }
      target.XmlElement.addChild(xmlChild)
    } catch (e) {
      throw new IllegalAccessException(e.Message + "Provided field's type: ${ObjectToConvert.Class}")
    }
  }

  private function getSimpleValue(object : Object) : XmlSimpleValue {
    if (object typeis String) {
      return XmlSimpleValue.makeStringInstance(object)
    }
    if (object typeis Integer) {
      return XmlSimpleValue.makeIntegerInstance(object)
    }
    if (object typeis BigDecimal) {
      return XmlSimpleValue.makeDecimalInstance(object)
    }
    if (object typeis Date) {
      return XmlSimpleValue.makeDateTimeInstance(new XmlDateTime(object.toCalendar(), false))
    }
    if (object typeis Float) {
      return XmlSimpleValue.makeFloatInstance(object)
    }
    if (object typeis Double) {
      return XmlSimpleValue.makeDoubleInstance(object)
    }
    if (object typeis Boolean) {
      return XmlSimpleValue.makeBooleanInstance(object)
    }
    if (object typeis Long) {
      return XmlSimpleValue.makeLongInstance(object)
    }
    return null
  }
}