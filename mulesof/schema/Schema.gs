package com.lv.nucleus.mulesoft.schema

interface Schema {
  public function toXml(coreObject : KeyableBean) : String
}