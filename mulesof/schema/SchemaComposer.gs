package com.lv.nucleus.mulesoft.schema

interface SchemaComposer {
  function composeSchema() : Schema
}