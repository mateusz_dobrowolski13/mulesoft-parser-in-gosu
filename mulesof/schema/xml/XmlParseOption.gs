package com.lv.nucleus.mulesoft.schema.xml

uses com.lv.nucleus.mulesoft.getter.IGetter

uses java.util.function.Predicate

final class XmlParseOption<T, R> {
  private var _fieldGetter : IGetter as FieldValueGetter
  private var _visibilityCondition : Predicate<T> as VisibilityCondtion

  construct() {
  }

  public function withValue(value : Object) : XmlParseOption {
    _fieldGetter = \object -> Optional.of(value)
    return this
  }

  public function withEntity(fieldGetter : IGetter) : XmlParseOption {
    _fieldGetter = fieldGetter
    return this
  }

  public function withVisibilityCondtion(visibilityCondtion : Predicate<T>) : XmlParseOption {
    _visibilityCondition = visibilityCondtion
    return this
  }

}