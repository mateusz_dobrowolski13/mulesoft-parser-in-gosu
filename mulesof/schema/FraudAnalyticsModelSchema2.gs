package com.lv.nucleus.mulesoft.schema

uses gw.api.financials.FinancialsCalculationUtil

internal class FraudAnalyticsModelSchema2 implements SchemaComposer {

  override function composeSchema() : SchemaBuilder {
    return new SchemaBuilder("FraudAnalyticsModelv2")
        .withFieldAndValue("ContactTime", Date.CurrentDate)
        .withField("Claim", "Claim")
        .withField("Claim.ClaimNumber", "ClaimNumber")
        .withField("Claim.LossDate", "LossDate")
        .withField("Claim.Policy_Brand", "Policy.Brand_Ext.DisplayName")
        .withField("Claim.Product_SubScheme", "Policy.ProductSubScheme_Ext.DisplayName")
        .withField("Claim.Policy_InceptionDate", "Policy.PolicyInceptionDate_Ext")
        .withField("Claim.Policy_ExpiryDate", "Policy.PolicyTadExpiryDate_Ext")
        .withFieldLambda("Claim.TotalPaid", \claim : Claim -> FinancialsCalculationUtil.getTotalPayments().getAmount(claim).getAmount())
        .withFieldLambda("Claim.TotalReserve", \claim : Claim -> FinancialsCalculationUtil.getTotalReserves().getAmount(claim).getAmount())
        .withField("Claim.LossCause", "LossCause")
        .withField("Claim.LossCause.Code", "Code")
        .withField("Claim.LossCause.Description", "Description")
        .withField("Claim.ReportedByType", "ReportedByType")
        .withField("Claim.ReportedByType.Code", "Code")
        .withField("Claim.ReportedByType.Description", "Description")
        .withField("Claim.LVFaultRatingView", "LVFaultRatingView")
        .withField("Claim.LVFaultRatingView.Code", "Code")
        .withField("Claim.LVFaultRatingView.Description", "Description")
        .withField("Claim.LossCircumstances_Ext", "LossCircumstances_Ext")
        .withField("Claim.LossCircumstances_Ext.Code", "Code")
        .withField("Claim.LossCircumstances_Ext.Description", "Description")
        .withField("Claim.HowReported", "HowReported")
        .withField("Claim.HowReported.Code", "Code")
        .withField("Claim.HowReported.Description", "Description")
        .withFieldLambda("Claim.Exposures", \claim : Claim -> claim.Exposures.where(\exposure -> checkExposure(exposure)))
        //exposures
        .withField("Claim.Exposures", "PublicID")
        .withField("Claim.Exposures.ClaimOrder", "ClaimOrder")
        .withField("Claim.Exposures.ExposureType", "ExposureType")
        .withField("Claim.Exposures.ExposureType.Code", "Code")
        .withField("Claim.Exposures.ExposureType.Description", "Description")
        .withField("Claim.Exposures.CoverageSubType", "CoverageSubType")
        .withField("Claim.Exposures.CoverageSubType.Code", "Code")
        .withField("Claim.Exposures.CoverageSubType.Description", "Description")
        .withFieldLambda("Claim.Exposures.Incident", \exposure : Exposure -> getAppropriateIncident(exposure))
        .withField("Claim.Exposures.Incident.NumberOfOccupants_Ext", "NumberOfOccupants_Ext", \incident : Incident -> incident typeis VehicleIncident)
            //driver
        .withField("Claim.Exposures.Incident.driver", "driver", \inc : Incident -> inc typeis VehicleIncident)
        .withField("Claim.Exposures.Incident.driver.PublicID", "PublicID")
        .withField("Claim.Exposures.Incident.driver.DateOfBirth", "DateOfBirth")
        .withField("Claim.Exposures.Incident.driver.Address", "PrimaryAddress")
        .withField("Claim.Exposures.Incident.driver.Address.PublicID", "PublicID")
        .withField("Claim.Exposures.Incident.driver.Address.PostCode", "PostalCode")
        .withField("Claim.Exposures.Incident.driver.Address.Country", "Country.DisplayName")
        .withField("Claim.Exposures.Incident.driver.Address.Longitude", "Longitude_Ext")
        .withField("Claim.Exposures.Incident.driver.Address.Latitutde", "Latitude_Ext")
            //vehicle
        .withField("Claim.Exposures.Incident.Vehicle", "Vehicle", \inc : Incident -> inc typeis VehicleIncident)
        .withField("Claim.Exposures.Incident.Vehicle.PublicID", "PublicID")
        .withField("Claim.Exposures.Incident.Vehicle.Make", "Make")
        .withField("Claim.Exposures.Incident.Vehicle.Model", "Model")
        .withField("Claim.Exposures.Incident.Vehicle.Year", "Year")
        .withField("Claim.Exposures.Incident.Vehicle.ABICode", "ABICode_Ext")

        .withFieldLambda("Claim.Exposures.Incident.Claimant", \inc : Incident -> inc.getClaimContactForClaimant_Ext())
        .withField("Claim.Exposures.Incident.Claimant.PublicID", "PublicID")
        .withField("Claim.Exposures.Incident.Claimant.DateOfBirth", "Contact.Person.DateOfBirth")
        .withField("Claim.Exposures.Incident.Claimant.Address", "Contact.PrimaryAddress")
        .withField("Claim.Exposures.Incident.Claimant.Address.PublicID", "PublicID")
        .withField("Claim.Exposures.Incident.Claimant.Address.PostCode", "PostalCode")
        .withField("Claim.Exposures.Incident.Claimant.Address.County", "County")
        .withField("Claim.Exposures.Incident.Claimant.Address.CountyDetails", "CountyDisplayName_Ext")
        .withField("Claim.Exposures.Incident.Claimant.Address.Longitude", "Longitude_Ext")
        .withField("Claim.Exposures.Incident.Claimant.Address.Latitutde", "Latitude_Ext")
  }

  function checkExposure(exposure : Exposure) : boolean {
    if (exposure.ExposureType == ExposureType.TC_VEHICLEDAMAGE) {
      return true
    } else if (exposure.LossParty == LossPartyType.TC_THIRD_PARTY) {
      if (exposure.ExposureType == ExposureType.TC_PROPERTYDAMAGE or exposure.ExposureType == ExposureType.TC_BODILYINJURYDAMAGE) {
        return true
      }
    }
    return false
  }

  function getAppropriateIncident(exposure : Exposure) : Incident {
    if (exposure.ExposureType == ExposureType.TC_VEHICLEDAMAGE) {
      return exposure.VehicleIncident
    }
    if (exposure.ExposureType == ExposureType.TC_BODILYINJURYDAMAGE) {
      return exposure.InjuryIncident
    }
    return exposure.PropertyIncident
  }
}