package com.lv.nucleus.mulesoft.getter

class GetterObjectFromStringPath<T> implements IGetter<T> {

  private var _path : String as Path

  construct(path : String) {
    _path = path
  }

  override function getObject(coreObject : T) : Optional {
    var paramObject : Object = coreObject
    for (path in _path.split("\\.")) {

      if (paramObject == null) {
        return Optional.empty()
      }

      var propertyInfo = (typeof paramObject).TypeInfo.getProperty(path)
      if (propertyInfo == null) {
        throw new IllegalAccessException("There is no field ${path} on " + paramObject.Class)
      }
      paramObject = propertyInfo.Accessor.getValue(paramObject)
    }
    return Optional.ofNullable(paramObject)
  }
}