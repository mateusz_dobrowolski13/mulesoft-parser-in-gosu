package com.lv.nucleus.mulesoft.conventer

uses com.lv.nucleus.mulesoft.schema.xml.XmlPayload

abstract class AbstractConventer<T> {
  protected var _objectToConvert : Object as ObjectToConvert

  construct(objectToConvert : Object) {
    this._objectToConvert = objectToConvert
  }

  abstract function convert(target : XmlPayload<T>)
}